const button = document.getElementById('button')
const email = document.getElementById('mail')
const checkbox = document.getElementById('suscribe')

function enabledButton(){
    const regularExpresion = /\S+@\S+\.\S+/;
    const isEmailValid = regularExpresion.test(email.value);
    const checkboxValue = checkbox.checked;
    
    if(isEmailValid && checkboxValue){
      button.removeAttribute('disabled');
    }else {
      button.setAttribute('disabled', 'disabled')
    }
}

function forSubmit(){
  console.log('hola')
  return alert('¡Perfecto, te avisaremos la primera!')
}

email.addEventListener('keyup', enabledButton);
email.addEventListener('change', enabledButton)
checkbox.addEventListener('change', enabledButton);
